﻿//Norman Nguyen
//Score Manager 2: Score Manager for the Player 2
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager2 : MonoBehaviour
{
    //Text
    public Text scoreText;

    public TankData data;
    // Use this for initialization
    void Start()
    {
        scoreText = GetComponent<Text>();
        data = GetComponent<TankData>();
        scoreText = GameObject.Find("P2Score").GetComponent<Text>();
        setUIScore();
    }

    // Update is called once per frame
    void Update()
    {
        setUIScore();
    }
    //UI
    private void setUIScore()
    {
        scoreText.text = "Score: " + data.currentScore.ToString();
    }
}
